#include <stdio.h>
#include <unistd.h> 
#include <semaphore.h> 	
#include <pthread.h> // para usar threads
#define TOPE 5
typedef enum{dormido=0,atendiendo=1} estado_barber;
sem_t cliente;
sem_t barbero;
sem_t corte;
sem_t asiento; 
pthread_mutex_t mutex;
int cant_clientes = 0; 
estado_barber estado; 
void* barber(void * arg) { 
      while(1){ 
       		sem_wait(&asiento);		
		if(cant_clientes==0){
			printf("ZZZ...\n");			
			estado=dormido;
			sem_post(&cliente);	/* bring customer for haircut.*/
			sem_wait(&barbero);	/* barber is cutting hair.*/
  	    	}

          	else {	

			estado=atendiendo;
			printf("cortando\n");
			cant_clientes--; 			
			printf("clientes en espera:%d\n",cant_clientes);
			sem_post(&cliente);	/* bring customer for haircut.*/			
			sleep(3);						
		        printf("corte finalizado\n");
			sem_post(&corte);
			if(cant_clientes==0){ 
			printf("VUELTA A DORMIR!!!\n"); 
			pthread_exit(NULL);
			}			

			sem_wait(&barbero);

			
			
	       }
      } 
} 
  
void* customer(void* arg) { 

      while(1) {        
	sem_wait(&cliente);  

        if(TOPE  > cant_clientes) {
			
			cant_clientes++;   
  			if(!estado){
					printf("entro cliente\n");  		                   	
					printf("despierta al barbero\n"); 
					
					sem_post(&barbero); 			
					sem_post(&asiento); 
					
			}
			else {

				printf("entro cliente\n");  
				sleep(1);	
				sem_post(&cliente);
				sem_post(&barbero); 
				sem_post(&asiento); 
				sem_wait(&corte);
				pthread_exit(NULL);
			
	            	}
	}
	else { 
			//release the lock 
			printf("no hay lugar, vuelva otro día\n");
			pthread_exit(NULL);

            } 



		
      } 
}
int main(){

		sem_init(&asiento,0,1);
		sem_init(&barbero,0,0);
		sem_init(&cliente,0,0);
		sem_init(&corte,0,0);
		pthread_mutex_init(&mutex,NULL);
		pthread_t p1; //una variable de tipo pthread_t sirve para identificar al hilo que se cree
		pthread_t p2[7];
		pthread_create(&p1, NULL, barber, NULL);
			
		for (int i=0; i<10; i++) {
			pthread_create(&p2[i], NULL, customer, NULL);
		}
		sem_destroy(&asiento);
		sem_destroy(&barbero);
		sem_destroy(&cliente);
		sem_destroy(&corte);
		pthread_exit(&p1);
} 

