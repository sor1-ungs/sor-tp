#include <stdio.h>
#include <semaphore.h>
#include <pthread.h>
#include <unistd.h> 
#include <stdlib.h> 
sem_t uno,dos,tres,cuatro,cinco,seis,activar;
int iteracion;
pthread_mutex_t m;
void * thread1 (void * arg){
	iteracion=*(int *)arg;
	while(1){	
		sem_wait(&uno);
		if(iteracion>0){			
			printf("Fiiiigaro\n");
			sem_post(&dos);	
		}
		else{
			sem_post(&dos);	
		        pthread_exit(NULL);
		}	
	}
}
void * thread2 (void * arg){
	while(1){
		sem_wait(&dos);	
		if(iteracion>0){	
			printf("Figaro ");
			sem_post(&tres);			
		}
		else{
				sem_post(&tres);	
			        pthread_exit(NULL);
		}
	}
}
void * thread3 (void * arg){
	while(1){	
		sem_wait(&tres);
		if(iteracion>0){	
			printf("Figaro ");
			sem_post(&cuatro);
		}
		else{
				sem_post(&cuatro);	
			        pthread_exit(NULL);
		}
	
	}
}
void * thread4 (void * arg){
	while(1){	
		sem_wait(&cuatro);
		if(iteracion>0){	
			printf("Figaro\n");
			sem_post(&cinco);
		}
		else{
				sem_post(&cinco);	
			        pthread_exit(NULL);
		}	
	}
}
void * thread5 (void * arg){
	while(1){	
		sem_wait(&cinco);
		if(iteracion>0){	
			printf("Figaro fi\n");
			sem_post(&seis);
		}
		else{
				sem_post(&seis);	
			        pthread_exit(NULL);
		}
	

	}
}
void * thread6 (void * arg){
	while(1){
		sem_wait(&seis);
		if(iteracion>0){	
			printf("Figaro fa\n");
			sem_post(&uno);}
		else{
			sem_post(&uno);	
		        pthread_exit(NULL);
		}
		iteracion--;
	}
}
int main(int argc,char*argv[]){
	//semaforos
	sem_init(&uno,0,1);
	sem_init(&dos,0,0);
	sem_init(&tres,0,0);
	sem_init(&cuatro,0,0);
	sem_init(&cinco,0,0);
	sem_init(&seis,0,0);

	int cant_veces_ejecutar=atoi(argv[1]);
	//threads
	pthread_t t1,t2,t3,t4,t5,t6;
	pthread_create(&t1,NULL,thread1,&cant_veces_ejecutar);

	pthread_create(&t2,NULL,thread2,NULL);
		
	pthread_create(&t3,NULL,thread3,NULL);
	
	pthread_create(&t4,NULL,thread4,NULL);
	
	pthread_create(&t5,NULL,thread5,NULL);
	
	pthread_create(&t6,NULL,thread6,NULL);
	//finalizacion de semaforos y threads
	sem_destroy(&uno);
	sem_destroy(&dos);
	sem_destroy(&tres);
	sem_destroy(&cuatro);
	sem_destroy(&cinco);
	sem_destroy(&seis);
     	pthread_exit(NULL);
	return 0;

}


